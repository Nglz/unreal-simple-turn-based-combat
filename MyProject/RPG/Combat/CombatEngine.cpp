// Fill out your copyright notice in the Description page of Project Settings.


#include "CombatEngine.h"

CombatEngine::CombatEngine(
	TArray<UGameCharacter*> playerParty,
	TArray<UGameCharacter*> enemyParty)
{
	PlayerParty = playerParty;
	EnemyParty = enemyParty;
	for (auto player : playerParty)
		CombatantOrder.Add(player);
	for (auto enemy : enemyParty)
		CombatantOrder.Add(enemy);

	for (const auto character : CombatantOrder)
		character->CombatInstance = this;
	
	this->TickTargetIndex = 0;
	SetPhase(CPHASE_Decision);
}

CombatEngine::~CombatEngine()
{
	for (auto enemy : EnemyParty)
		enemy = nullptr;

	for (auto combatant : CombatantOrder)
		combatant = nullptr;
}

bool CombatEngine::Tick(float DeltaSeconds)
{
	switch (CombatPhase)
	{
	case CPHASE_Decision:
		// Ask player what to do
		if (!waitingForCharacter)
		{
			CurrentTickTarget->BeginMakeDecision();
			waitingForCharacter = true;
		}
		if (CurrentTickTarget->MakeDecision(DeltaSeconds)) // decision has been made
		{
			SelectNextCharacter();
			waitingForCharacter = false;
			// change phase if no more player character
			if (TickTargetIndex == -1)
				SetPhase(CPHASE_Action);
		}
		break;
	case CPHASE_Action:
		// Do selected action
		if (!waitingForCharacter)
		{
			CurrentTickTarget->BeginExecuteAction();
			waitingForCharacter = true;
		}
		if (CurrentTickTarget->ExecuteAction(DeltaSeconds)) // decision has been made
			{
			SelectNextCharacter();
			// change phase if no more character
			if (TickTargetIndex == -1)
				SetPhase(CPHASE_Decision);
			}
		break;
	case CPHASE_Victory:
	case CPHASE_GameOver:
		return true;
	}

	// check for game over
	int deadCount = 0;
	for (auto player : PlayerParty)
	{
		if (!player->IsAlive()) ++deadCount;
	}

	if (deadCount == PlayerParty.Num())
	{
		this->SetPhase(CPHASE_GameOver);
		return false;
	}
	
	// check for victory
	deadCount = 0;
	for (auto enemy : EnemyParty)
	{
		if (!enemy->IsAlive()) ++deadCount;
	}

	if (deadCount == EnemyParty.Num())
	{
		this->SetPhase(CPHASE_Victory);
		return false;
	}
	return false;
}

void CombatEngine::SetPhase(ECombatPhase phase)
{
	CombatPhase = phase;
	switch (phase)
	{
	case CPHASE_Decision:
	case CPHASE_Action:
		TickTargetIndex = 0;
		SelectNextCharacter();
		break;
	case CPHASE_Victory:
		// handle victory
		break;
	case CPHASE_GameOver:
		//handle game over
		break;
	}
}

void CombatEngine::SelectNextCharacter()
{
	waitingForCharacter = false;
	
	// select next non dead character in the order
	for (int i = TickTargetIndex; i<CombatantOrder.Num(); ++i)
	{
		auto character = CombatantOrder[i];

		if (character->IsAlive())
		{
			TickTargetIndex= i+1;
			CurrentTickTarget = character;
			return;
		}
	}

	TickTargetIndex = -1;
	CurrentTickTarget = nullptr;
}
