// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyProject/RPG/GameCharacter.h"

enum ECombatPhase : uint8
{
	CPHASE_Decision,
	CPHASE_Action,
	CPHASE_Victory,
	CPHASE_GameOver,
};

class MYPROJECT_API CombatEngine
{
public:

	TArray<UGameCharacter*> CombatantOrder;
	TArray<UGameCharacter*> PlayerParty;
	TArray<UGameCharacter*> EnemyParty;

	ECombatPhase CombatPhase;

protected:

	UGameCharacter* CurrentTickTarget;
	int TickTargetIndex;
	bool waitingForCharacter;

public:
	
	CombatEngine(TArray<UGameCharacter*> playerParty, TArray<UGameCharacter*> enemyParty);
	~CombatEngine();

	bool Tick(float DeltaSeconds);

protected:
	void SetPhase(ECombatPhase phase);
	void SelectNextCharacter();
};
