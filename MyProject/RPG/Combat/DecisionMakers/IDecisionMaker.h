#pragma once

#include "MyProject/RPG/GameCharacter.h"
class UGameCharacter;

class IDecisionMaker
{
public:
	virtual ~IDecisionMaker() = default;
	virtual void BeginMakeDecision(UGameCharacter* character) = 0;
	virtual bool MakeDecision(float deltaSeconds) = 0;
};
