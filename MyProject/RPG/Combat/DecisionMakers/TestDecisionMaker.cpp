#include "TestDecisionMaker.h"

#include "MyProject/RPG/Combat/Actions/TestCombatAction.h"

void TestDecisionMaker::BeginMakeDecision(UGameCharacter* character)
{
	UE_LOG(LogTemp, Log, TEXT("Character %s is DECIDING."), *character->CharacterName);
	character->CombatAction = new TestCombatAction( character->SelectTarget());
}

bool TestDecisionMaker::MakeDecision(float deltaSeconds)
{
	return true;
}

