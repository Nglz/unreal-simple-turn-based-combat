#pragma once
#include "IDecisionMaker.h"

class TestDecisionMaker : public IDecisionMaker
{
public:
	virtual void BeginMakeDecision(UGameCharacter* character)override;
	virtual bool MakeDecision(float deltaSeconds) override;
};
