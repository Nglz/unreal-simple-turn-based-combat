#include "TestCombatAction.h"


TestCombatAction::TestCombatAction(UGameCharacter* target):
	DelayTimer(1.0f),
	Character(nullptr),
	Target(target){}

void TestCombatAction::BeginExecuteAction(UGameCharacter* character)
{
	Character = character;
	if (!Target->IsAlive())
		Target = Character->SelectTarget();
	if (Target == nullptr)
		return;
	// DelayTimer = 1.0f;
	
	UE_LOG(LogTemp, Log, TEXT("Character %s attacks %s"), *character->CharacterName, *Target->CharacterName);
	Target->HP-=Character->ATK;
}

bool TestCombatAction::ExecuteAction(float deltaSeconds)
{
	this->DelayTimer -= deltaSeconds;
	return DelayTimer <= 0;
}
