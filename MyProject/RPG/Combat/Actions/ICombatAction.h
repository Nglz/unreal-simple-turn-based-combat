#pragma once
#include "MyProject/RPG/GameCharacter.h"

class UGameCharacter;


class ICombatAction
{
public:
	virtual ~ICombatAction() = default;
	virtual void BeginExecuteAction(UGameCharacter* character) = 0;
	virtual bool ExecuteAction(float deltaSeconds) = 0;
};
