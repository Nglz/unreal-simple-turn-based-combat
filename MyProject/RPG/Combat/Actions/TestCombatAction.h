#pragma once
#include "ICombatAction.h"
#include "MyProject/RPG/GameCharacter.h"

class TestCombatAction : public ICombatAction
{

protected:
	float DelayTimer; //testing purposes
	UGameCharacter* Character;
	UGameCharacter* Target;
	
public:
	explicit TestCombatAction(UGameCharacter* target);
	virtual void BeginExecuteAction(UGameCharacter* character) override;
	virtual bool ExecuteAction(float deltaSeconds) override;
	
};
