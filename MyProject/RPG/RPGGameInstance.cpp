// Fill out your copyright notice in the Description page of Project Settings.


#include "RPGGameInstance.h"

URPGGameInstance::URPGGameInstance(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	IsInitialized = false;
}

void URPGGameInstance::Init()
{
	if (this->IsInitialized) return;

	const UDataTable* characters = Cast<UDataTable>(
		StaticLoadObject(UDataTable::StaticClass(), nullptr,
		                 TEXT(
			                 "DataTable'/Game/CustomContent/Data/Characters.Characters'")));

	if (characters == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Could not find characters data."));
		return;
	}

	FCharacterInfo* chara = characters->FindRow<FCharacterInfo>(
		TEXT("Kim"),
		TEXT("LookupCharacterClass"));
	if (chara == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Character 'Kim' does not exist in data"));
		return;
	}

	this->PartyMembers.Add(UGameCharacter::CreateGameCharacter(chara, this));

	this->IsInitialized = true;
	UE_LOG(LogTemp, Log, TEXT("Instance initialized"));
}
