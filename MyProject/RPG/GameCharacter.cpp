// Fill out your copyright notice in the Description page of Project Settings.

#include "GameCharacter.h"

#include "Combat/CombatEngine.h"
#include "Combat/Actions/TestCombatAction.h"
#include "Combat/DecisionMakers/TestDecisionMaker.h"

UGameCharacter* UGameCharacter::CreateGameCharacter(
	FCharacterInfo* characterInfo,
	UObject* Outer)
{
	UGameCharacter* character = NewObject<UGameCharacter>(Outer);
	const UDataTable* characterClasses = Cast<UDataTable>(
		StaticLoadObject(UDataTable::StaticClass(),
		                 NULL,
		                 TEXT(
			                 "DataTable'/Game/CustomContent/Data/CharacterClasses.CharacterClasses'")));

	if (characterClasses == NULL)
	{
		UE_LOG(LogTemp, Error, TEXT("Character classes data not found."));
		return nullptr;
	}

	character->CharacterName = characterInfo->Character_Name;
	FCharacterClassInfo* charClass = characterClasses->FindRow<FCharacterClassInfo>(
		*(characterInfo->Class_ID), TEXT("LookupCharacterClass"));
	character->ClassInfo = charClass;

	character->MHP = character->ClassInfo->StartMHP;
	character->MMP = character->ClassInfo->StartMMP;
	character->HP = character->MHP;
	character->MP = character->MMP;
	character->Luck = character->ClassInfo->StartLuck;
	character->ATK = character->ClassInfo->StartATK;
	character->DEF = character->ClassInfo->StartDEF;
	// character->DecisionMaker = new TestDecisionMaker();
	character->IsPlayer = true;

	return character;
}

UGameCharacter* UGameCharacter::CreateGameCharacter(const FEnemyInfo* EnemyInfo, UObject* Outer)
{
	UGameCharacter* character = NewObject<UGameCharacter>(Outer);

	character->CharacterName = EnemyInfo->Enemy_Name;
	character->ClassInfo = nullptr;
	
	character->MHP = EnemyInfo->MHP;
	character->HP = character->MHP;
	character->Luck = EnemyInfo->Luck;
	character->ATK = EnemyInfo->ATK;
	character->DEF = EnemyInfo->DEF;
	character->DecisionMaker = new TestDecisionMaker();
	character->IsPlayer = false;

	return character;
}

UGameCharacter* UGameCharacter::SelectTarget() const
{
	UGameCharacter* target = nullptr;
	TArray<UGameCharacter*> targetList;
	if (IsPlayer)
		targetList = CombatInstance->EnemyParty;
	else
		targetList = CombatInstance->PlayerParty;

	for (const auto chara : targetList)
		if (chara->IsAlive())
		{
			target = chara;
			break;
		}

	if (!target->IsAlive())
		return nullptr;

	return target;
}

void UGameCharacter::BeginMakeDecision()
{
	DecisionMaker->BeginMakeDecision(this);
}

bool UGameCharacter::MakeDecision(float deltaSeconds) const
{
	return DecisionMaker->MakeDecision(deltaSeconds);
}

void UGameCharacter::BeginExecuteAction()
{
	CombatAction->BeginExecuteAction(this);
}

bool UGameCharacter::ExecuteAction(float deltaSeconds) const
{
	if (CombatAction->ExecuteAction(deltaSeconds))
	{
		delete CombatAction;
		return true;
	}
	return false;
}

void UGameCharacter::BeginDestroy()
{
	Super::BeginDestroy();
	if (!this->IsPlayer)
		delete DecisionMaker;
}
