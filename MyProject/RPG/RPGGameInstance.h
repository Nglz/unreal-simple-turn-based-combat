// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "GameCharacter.h"
#include "RPGGameInstance.generated.h"

UCLASS()
class MYPROJECT_API URPGGameInstance : public UGameInstance
{
	GENERATED_BODY()

	explicit URPGGameInstance(const class FObjectInitializer& ObjectInitializer);

public:
	UPROPERTY()
	TArray<UGameCharacter*> PartyMembers;

protected:
	bool IsInitialized;

public:
	virtual void Init() override;
};
