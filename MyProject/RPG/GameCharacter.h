// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Combat/Actions/ICombatAction.h"
#include "Combat/DecisionMakers/IDecisionMaker.h"
#include "Data/FCharacterInfo.h"
#include "Data/FCharacterClassInfo.h"
#include "Data/FEnemyInfo.h"
#include "GameCharacter.generated.h"

class CombatEngine;
class ICombatAction;
class IDecisionMaker;

UCLASS(BlueprintType)
class MYPROJECT_API UGameCharacter final : public UObject
{
	GENERATED_BODY()

public:
	bool IsPlayer;
	FCharacterClassInfo* ClassInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=CharacterInfo)
	FString CharacterName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=CharacterInfo)
	int32 MHP;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=CharacterInfo)
	int32 HP;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=CharacterInfo)
	int32 MMP;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=CharacterInfo)
	int32 MP;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=CharacterInfo)
	int32 ATK;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=CharacterInfo)
	int32 DEF;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=CharacterInfo)
	int32 Luck;
	CombatEngine* CombatInstance;
	
	ICombatAction* CombatAction; // Changes for each combat
	IDecisionMaker* DecisionMaker; // per character
	
public:
	static UGameCharacter* CreateGameCharacter(FCharacterInfo* characterInfo, UObject* Outer);
	static UGameCharacter* CreateGameCharacter(const FEnemyInfo* CharacterInfo, UObject* Outer);

	UFUNCTION(BlueprintCallable, Category=CharacterInfo)
	bool IsAlive() const { return HP > 0; }
	UGameCharacter* SelectTarget() const;
	void BeginMakeDecision();
	bool MakeDecision(float deltaSeconds) const;
	void BeginExecuteAction();
	bool ExecuteAction(float deltaSeconds) const;

	virtual void BeginDestroy() override;
};
