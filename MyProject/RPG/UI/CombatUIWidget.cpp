// Fill out your copyright notice in the Description page of Project Settings.


#include "CombatUIWidget.h"

#include "MyProject/RPG/Combat/CombatEngine.h"
#include "MyProject/RPG/Combat/Actions/TestCombatAction.h"

TArray<UGameCharacter*> UCombatUIWidget::GetCharacterTargets()
{
	if (CurrentTarget->IsPlayer)
		return CurrentTarget->CombatInstance->EnemyParty;
	else
		return CurrentTarget->CombatInstance->PlayerParty;
}

void UCombatUIWidget::AttackTarget(UGameCharacter* target)
{
	TestCombatAction* action = new TestCombatAction(target);
	CurrentTarget->CombatAction = action;
	DecisionMade = true;
}

void UCombatUIWidget::BeginMakeDecision(UGameCharacter* target)
{
	CurrentTarget = target;
	DecisionMade = false;

	ShowActionPanel(target);
}

bool UCombatUIWidget::MakeDecision(float deltaSeconds)
{
	return DecisionMade;
}


