// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MyProject/RPG/GameCharacter.h"
#include "CombatUIWidget.generated.h"

UCLASS()
class MYPROJECT_API UCombatUIWidget : public UUserWidget, public IDecisionMaker
{
	GENERATED_BODY()
protected:
	UGameCharacter* CurrentTarget;
	bool DecisionMade;
	
public:
	UFUNCTION(BlueprintImplementableEvent, Category=CombatUI)
	void AddPlayerCharacterPanel (UGameCharacter* target);

	UFUNCTION(BlueprintImplementableEvent, Category=CombatUI)
	void AddEnemyCharacterPanel (UGameCharacter* target);

	UFUNCTION(BlueprintImplementableEvent, Category=CombatUI)
	void ShowActionPanel(UGameCharacter* target);
	
	UFUNCTION(BlueprintCallable, Category=CombatUI)
	TArray<UGameCharacter*> GetCharacterTargets();

	UFUNCTION(BlueprintCallable, Category=CombatUI)
	void AttackTarget(UGameCharacter* target);

	virtual void BeginMakeDecision(UGameCharacter* target) override;
	virtual bool MakeDecision(float deltaSeconds) override;
};
