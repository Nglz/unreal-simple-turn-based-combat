// Fill out your copyright notice in the Description page of Project Settings.


#include "RPGGameMode.h"
#include "RPGCharacter.h"
#include "RPG/Combat/CombatEngine.h"
#include "Kismet/GameplayStatics.h"
#include "RPG/RPGGameInstance.h"

ARPGGameMode::ARPGGameMode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	DefaultPawnClass = ARPGCharacter::StaticClass();
	PrimaryActorTick.bCanEverTick = true;
}

void ARPGGameMode::BeginPlay()
{
	UE_LOG(LogTemp, Log, TEXT("BeginPlay() | RPGGameMode"));
	Cast<URPGGameInstance>(GetGameInstance())->Init();
}

void ARPGGameMode::Tick(float DeltaTime)
{
	if (CurrentCombatInstance == nullptr)
		return;
	
	if (CurrentCombatInstance->Tick(DeltaTime)) // Combat is over if tick returns true
	{
		switch(CurrentCombatInstance->CombatPhase)
		{
		case CPHASE_GameOver:
			UE_LOG(LogTemp, Log, TEXT("Player has lost combat. Game over."));
			break;
		case CPHASE_Victory:
			UE_LOG(LogTemp, Log, TEXT("Player has won! Hurray!"));
			break;
		default:
			UE_LOG(LogTemp, Log, TEXT("Combat has ended but it's not a victory or a defeat?"));
			break;
		}

		// Freeeeee
		UGameplayStatics::GetPlayerController(GetWorld(), 0)->SetActorTickEnabled(true);
		for (const auto character : CurrentCombatInstance->PlayerParty)
			character->DecisionMaker = nullptr;
		CombatUIInstance->RemoveFromViewport();
		CombatUIInstance = nullptr;
		delete(CurrentCombatInstance);
		CurrentCombatInstance = nullptr;
		EnemyParty.Empty();
	}
}

void ARPGGameMode::TestCombat()
{
	// get enemies data
	const UDataTable* enemyData = Cast<UDataTable>(
		StaticLoadObject(UDataTable::StaticClass(), nullptr,
						 TEXT(
							 "DataTable'/Game/CustomContent/Data/Enemies.Enemies'")));

	if (enemyData == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Enemy data not found"));
		return;
	}
	const FEnemyInfo* enemyInfo = enemyData->FindRow<FEnemyInfo>(TEXT("Goblin"), TEXT("LookupEnemyInfo"));

	if (enemyInfo == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Enemy 'Goblin' does not exist"));
		return;
	}
	// disable player actor
	UGameplayStatics::GetPlayerController(GetWorld(), 0)->SetActorTickEnabled(false);

	// add enemies
	UGameCharacter* enemyCharacter = UGameCharacter::CreateGameCharacter(enemyInfo, this);
	EnemyParty.Add(enemyCharacter);

	const URPGGameInstance* GameInstance = Cast<URPGGameInstance>(GetGameInstance());

	CurrentCombatInstance = new CombatEngine(GameInstance->PartyMembers, EnemyParty);

	UE_LOG(LogTemp, Log, TEXT("Combat started"));

	// UI stuff
	CombatUIInstance = CreateWidget<UCombatUIWidget>(GetGameInstance(), CombatUIClass);
	if (CombatUIInstance == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Unable to create combat widget"));
		return;
	}
	
	CombatUIInstance->AddToViewport();

	UGameplayStatics::GetPlayerController(GetWorld(), 0)->bShowMouseCursor = true;
	for (const auto character : GameInstance->PartyMembers)
	{
		CombatUIInstance->AddPlayerCharacterPanel(character);
		character->DecisionMaker = CombatUIInstance;
	}
	for (const auto chara : EnemyParty)
		CombatUIInstance->AddEnemyCharacterPanel(chara);
	
}
