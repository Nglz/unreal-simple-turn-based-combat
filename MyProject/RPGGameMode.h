// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "RPG/UI/CombatUIWidget.h"
#include "MyProject.h"
#include "RPGGameMode.generated.h"

UCLASS()
class MYPROJECT_API ARPGGameMode final : public AGameModeBase
{
	GENERATED_BODY()

	explicit ARPGGameMode(const FObjectInitializer& ObjectInitializer);
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

public:
	CombatEngine* CurrentCombatInstance;
	UPROPERTY()
	TArray<UGameCharacter*> EnemyParty;
	UPROPERTY()
	UCombatUIWidget* CombatUIInstance;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category=UI)
	TSubclassOf<UCombatUIWidget> CombatUIClass;

	UFUNCTION(exec)
	void TestCombat(); // As name suggests, testing purposes.
};
